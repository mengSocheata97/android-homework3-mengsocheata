package com.cheata.hw3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

        Toolbar toolbar1;
        private Context mContext;
        private LinearLayout linearLayout;
        Menu saveMenu;
        PopupWindow popupWindow;
        Button btnSave,btnCancel;
        EditText editTitle, editDes;
        String title, description;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            mContext = getApplicationContext();
            toolbar1 = findViewById(R.id.toolbar);
            linearLayout = findViewById(R.id.main_linear);
            setSupportActionBar(toolbar1);
        }
//        menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_main,menu);
            saveMenu = menu;
            return true;
    }
//    item in menu
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.add : showPopUp();break;
            case R.id.remove : removeAllList();break;
            case R.id.fade_in: Intent intent = new Intent(this, FadeInAnimation.class);
                            startActivity(intent);break;
            case R.id.fade_out: Intent intent1 = new Intent(this, FadeOutAnimation.class);
                            startActivity(intent1);break;
            case R.id.rotate: Intent intent2 = new Intent(this, RotateAnimation.class);
                            startActivity(intent2);break;
            case R.id.zoom : Intent intent3 = new Intent(this, ZoomAnimation.class);
                            startActivity(intent3);break;
        }
        return true;
    }
//    remove all list
    public void removeAllList()
    {
        if (linearLayout.getChildCount()>0)
            linearLayout.removeAllViews();
    }
//    show my popup
    public void showPopUp()
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View customView = inflater.inflate(R.layout.pop_up,null,false);
        popupWindow = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true
        );
        popupWindow.setAnimationStyle(R.style.animate);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER,0,0);

        editTitle = customView.findViewById(R.id.txt_title);
        editDes = customView.findViewById(R.id.txt_description);
        btnCancel = customView.findViewById(R.id.btn_cancel);
        btnSave = customView.findViewById(R.id.btn_save);
//    btnSave clicked
        btnSave.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                title = editTitle.getText().toString();
                description = editDes.getText().toString();
               if(!editTitle.getText().toString().equals(" ") && !editDes.getText().toString().equals(""))
               {
                   saveMenu.setGroupEnabled(0,true);
                   showList(title,description);
                   Toast.makeText(MainActivity.this,"Success",Toast.LENGTH_LONG).show();
                   popupWindow.dismiss();
               }
               else
               {
                   Toast.makeText(MainActivity.this,"Please fill in the blanket",Toast.LENGTH_LONG).show();
               }
            }
        });
//            btnCancel clicked
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveMenu.setGroupEnabled(0,true);
                popupWindow.dismiss();
            }
        });
    }
//    show list
    @SuppressLint("ResourceAsColor")
    public void showList(String title, String description)
    {
        LinearLayout layout = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5,10,5,0);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setBackgroundColor(getResources().getColor(R.color.colorLightBlue1));

        TextView textView = new TextView(this);
        textView.setTextSize(26);
        textView.setTextColor(R.color.colorDarkBlue2);
        textView.setText(title);

        TextView textView1 = new TextView(this);
        textView1.setTextSize(20);
        textView1.setText(description);

        layout.addView(textView);
        layout.addView(textView1);
        linearLayout.addView(layout);
    }
}