package com.cheata.hw3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class FadeOutAnimation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade_out_animation);

        ImageView image = findViewById(R.id.fade_out);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_out);
        image.startAnimation(animation);
    }
}
